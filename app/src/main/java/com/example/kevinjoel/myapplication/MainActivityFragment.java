package com.example.kevinjoel.myapplication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private Button p1poisonmore;
    private ImageButton lifetwotoone;
    private ImageButton lifeonetotwo;
    private Button button;
    private Button p1poisonless;
    private Button p2poisonmore;
    private Button p2poisonless;
    private ImageButton p1lifemore;
    private ImageButton p1lifeless;
    private ImageButton p2lifemore;
    private ImageButton p2lifeless;
    private TextView counter1;
    private TextView counter2;

    private int life1;
    private int life2;
    private int poison1;
    private int poison2;
    private View view;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);


        p1poisonmore =  view.findViewById(R.id.p1poisonmore);
        lifetwotoone =  view.findViewById(R.id.lifetwotoone);
        lifeonetotwo =  view.findViewById(R.id.lifeonetotwo);
        p1poisonless =  view.findViewById(R.id.p1poisonless);
        p2poisonmore =  view.findViewById(R.id.p2poisonmore);
        p2poisonless =  view.findViewById(R.id.p2poisonless);
        p1lifemore =  view.findViewById(R.id.p1lifemore);
        p1lifeless = view.findViewById(R.id.p1lifeless);
        p2lifemore =  view.findViewById(R.id.p2lifemore);
        p2lifeless = view.findViewById(R.id.p2lifeless);
        counter1 = view.findViewById((R.id.counter1));
        counter2 = view.findViewById(R.id.counter2);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.lifeonetotwo:
                        life1--;
                        life2++;
                        break;

                    case R.id.lifetwotoone:
                        life1++;
                        life2--;
                        break;

                    case R.id.p1poisonmore:
                        poison1++;
                        break;

                    case R.id.p2poisonmore:
                        poison2++;
                        break;

                    case R.id.p1poisonless:
                        poison1--;
                        break;

                    case R.id.p2poisonless:
                        poison2--;
                        break;

                    case R.id.p1lifemore:
                        life1++;
                        break;

                    case R.id.p2lifemore:
                        life2++;
                        break;

                    case R.id.p1lifeless:
                        life1--;
                        break;

                    case R.id.p2lifeless:
                        life2--;
                        break;

                }

                updateViews();
            }
        };

        if (savedInstanceState != null) {
            poison1 = savedInstanceState.getInt("poison1");
            poison2 = savedInstanceState.getInt("poison2");
            life1 = savedInstanceState.getInt("life1");
            life2 = savedInstanceState.getInt("life2");
            updateViews();

        } else {
            reset();
        }


        lifetwotoone.setOnClickListener(listener);
        lifeonetotwo.setOnClickListener(listener);
        p1poisonmore.setOnClickListener(listener);
        p1poisonless.setOnClickListener(listener);
        p2poisonmore.setOnClickListener(listener);
        p2poisonless.setOnClickListener(listener);
        p1lifemore.setOnClickListener(listener);
        p1lifeless.setOnClickListener(listener);
        p2lifemore.setOnClickListener(listener);
        p2lifeless.setOnClickListener(listener);
        counter1.setOnClickListener(listener);
        counter2.setOnClickListener(listener);


        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.reset){
            reset();
            Snackbar.make(view, "New Game!", Snackbar.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    private void reset() {
        poison1 = 0;
        poison2 = 0;
        life1 = 20;
        life2 = 20;

        updateViews();
    }

    private void updateViews(){

        counter1.setText(String.format("%d/%d", life1, poison1));
        counter2.setText(String.format("%d/%d", life2, poison2));
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {

        savedInstanceState.putInt("poison1", poison1);
        savedInstanceState.putInt("poison2", poison2);
        savedInstanceState.putInt("life1", life1);
        savedInstanceState.putInt("life2", life2);



        super.onSaveInstanceState(savedInstanceState);
    }



}
